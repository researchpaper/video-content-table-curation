import os
import re
import sys
import html
import librosa
import numpy as np
from webvtt import WebVTT
from scipy.stats import kurtosis, skew
from pysrt.srtitem import SubRipItem, SubRipTime


def convert_to_srt(vtt_file):
    if not os.path.isfile(vtt_file):
        sys.stderr.write("File couldn't found %s.\n" % vtt_file)
        return
    else:
        file_name, file_extension = os.path.splitext(vtt_file)
        if not file_extension.lower() == ".vtt":
            sys.stderr.write("Skipping %s.\n" % vtt_file)
        index = 0
        srt = open(file_name + ".srt", "w")

        for caption in WebVTT().read(vtt_file):
            index += 1
            start = SubRipTime(0, 0, caption.start_in_seconds)
            end = SubRipTime(0, 0, caption.end_in_seconds)
            srt.write(SubRipItem(
                index, start, end, html.unescape(caption.text))
                .__str__()+"\n")


def extract_audio_features(audio_file):
    raw_sound, sample_rate = librosa.load(audio_file)
    mfccs = librosa.feature.mfcc(y=raw_sound, sr=sample_rate, n_mfcc=20)

    mfccs_min = np.min(mfccs, axis=1)  # row-wise summaries
    mfccs_max = np.max(mfccs, axis=1)
    mfccs_median = np.median(mfccs, axis=1)
    mfccs_mean = np.mean(mfccs, axis=1)
    mfccs_variance = np.var(mfccs, axis=1)
    mfccs_skeweness = skew(mfccs, axis=1)
    mfccs_kurtosis = kurtosis(mfccs, axis=1)

    features = np.concatenate([mfccs_min, mfccs_max, mfccs_median, mfccs_mean,
                               mfccs_variance, mfccs_skeweness, mfccs_kurtosis])
    return features


def text_preprocessing(text):
    ''' Pre process and convert texts to a list of words '''
    text = str(text)
    text = text.lower()
    # Clean the text
    text = re.sub(r"[^A-Za-z0-9^,!.\/'+-=]", " ", text)
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"can't", "cannot ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\^", " ^ ", text)
    text = re.sub(r"\+", " + ", text)
    text = re.sub(r"\-", " - ", text)
    text = re.sub(r"\=", " = ", text)
    text = re.sub(r"'", " ", text)
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", " : ", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)

    return text


def srttime_to_milliseconds(time):
    time = [int(i) for i in time.split(':')]
    return ((time[0]*3600) + (time[1]*60) + time[2]) * 1000
