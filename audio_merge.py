from utils import extract_audio_features
import os
from pydub import AudioSegment
audio1 = AudioSegment.from_mp3('audio_chunks/audio_chunk_0:0:0,0:1:13.mp3')
audio2 = AudioSegment.from_mp3('audio_chunks/audio_chunk_0:1:13,0:2:16.mp3')
audio = audio1 + audio2
audio.export('audio.mp3', format='mp3')
f1 = extract_audio_features('audio_chunks/audio_chunk_0:0:0,0:1:13.mp3')
f2 = extract_audio_features('audio_chunks/audio_chunk_0:1:13,0:2:16.mp3')
f = extract_audio_features('audio.mp3')
for i in range(len(f1)):
    print(f1[i], f2[i], f[i])
