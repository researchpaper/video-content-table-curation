#!/usr/bin/env python
# coding: utf-8

# In[111]:


from __future__ import unicode_literals

import os
import re
import glob
import nltk
import pysrt
import shutil
import sklearn
from utils import *
from pydub import AudioSegment
from nltk.corpus import wordnet as wn
from sumy.nlp.tokenizers import Tokenizer
from gensim.summarization import keywords
from scipy.spatial.distance import cosine
from sumy.summarizers.lsa import LsaSummarizer
from nltk.stem.wordnet import WordNetLemmatizer
from sumy.summarizers.luhn import LuhnSummarizer
from sumy.parsers.plaintext import PlaintextParser
from sumy.summarizers.lex_rank import LexRankSummarizer
from sumy.summarizers.text_rank import TextRankSummarizer
from sklearn.decomposition import LatentDirichletAllocation as LDA
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer


# ### Youtube URL input

# In[112]:


# video_url = input("Please enter youtube video link:")
video_url = "https://www.youtube.com/watch?v=DAOcjicFr1Y&t=6s"
n_id = video_url.find("&")
if n_id != -1:
    video_url = video_url[:n_id]


# ### Download subtitle and clean

# In[113]:


cmd = ["youtube-dl",
       "--skip-download",
       "--write-sub",
       "--write-auto-sub",
       "--quiet",
       "--sub-lang",
       "en",
       video_url
       ]

op_log = os.system(" ".join(cmd))
print(op_log)
if op_log != 0:
    print("Please enter a valid Youtube video link, which has english subtitle.")

directory = os.getcwd()
file_name = ""
for f in os.listdir(directory):
    if f.endswith(".vtt"):
        file_name = f
        break
convert_to_srt(file_name)
os.remove(file_name)


files = os.listdir('./')
for file in files:
    if file.endswith('.srt'):
        text_file = file
print(text_file)


# ### Download audio and clean

# In[64]:


cmd = ["youtube-dl",
       "-f",
       "140",
       video_url
       ]

op_log = os.system(" ".join(cmd))
print(op_log)

audio = ''
files = os.listdir('./')
for file in files:
    if file.endswith('m4a'):
        audio = file
if audio == '':
    print("Please enter a valid Youtube video link")
else:
    tmp_audio = AudioSegment.from_file(audio, format=audio.split(".")[-1])
    tmp_audio.export(audio.split(".")[0]+".mp3", format="mp3")
    os.remove(audio)
    audio_file = audio.split(".")[0]+".mp3"


# ### Fixed variables

# In[114]:


n_samples = 2000
n_features = 1000
n_components = 1
n_top_words = 25
threshold = 0.04
lambda_1 = 0.5
n_slice = 25
en_stop = set(nltk.corpus.stopwords.words('english'))


# ### Some important functions

# In[116]:


def get_lemma(word):
    lemma = wn.morphy(word)
    if lemma is None:
        return word
    else:
        return lemma


def prepare_text_for_lda(text):
    tokens = text.split()
    tokens = [token for token in tokens if len(token) > 4]
    tokens = [token for token in tokens if token not in en_stop]
    tokens = [get_lemma(token) for token in tokens]
    return tokens


def print_top_words(model, feature_names, n_top_words):
    mess = []
    for topic_idx, topic in enumerate(model.components_):
        message = " ".join([feature_names[i]
                            for i in topic.argsort()[:-n_top_words - 1:-1]])
        mess.append(message)
    return mess


def jaccard_similarity(list1, list2):
    intersection = len(list(set(list1).intersection(list2)))
    union = (len(list1) + len(list2)) - intersection
    return float(intersection) / float(union)


def get_sliced_audio(duration):
    if not os.path.exists('./audio_chunks/'):
        os.makedirs('./audio_chunks/')
    audio = AudioSegment.from_mp3(audio_file)
    beg = srttime_to_milliseconds(duration.split(',')[0])
    end = srttime_to_milliseconds(duration.split(',')[1])
    audio_split = audio[beg:end]
    audio_split.export('./audio_chunks/audio_chunk_'+duration+'.mp3')


def get_audio_similarity(duration1, duration2):
    audio_1 = './audio_chunks/audio_chunk_'+duration1+'.mp3'
    audio_2 = './audio_chunks/audio_chunk_'+duration2+'.mp3'
    features_1 = extract_audio_features(audio_1)
    features_2 = extract_audio_features(audio_2)
    return 1 - cosine(features_1, features_2)


def get_summary(text, summarizer):
    summary_sentences = ''
    parser = PlaintextParser.from_string(text, Tokenizer("english"))
    summary = summarizer(parser.document, 7)
    for sentence in summary:
        summary_sentences += str(sentence)
    return summary_sentences


# ### Returns the LDA results with timestamp

# In[117]:


def get_LDA_results(mega_subs, n_top_words=n_top_words, final_result=True):
    mega_subs_LDA = []
    tf_vectorizer = CountVectorizer(max_df=1.0,
                                    min_df=1,
                                    max_features=n_features,
                                    stop_words='english')

    for sub_id in range(len(mega_subs)):
        sub_text_tf = tf_vectorizer.fit_transform(mega_subs[sub_id][0])

        # Fit the LDA Model
        lda = LDA(n_components=n_components,
                  max_iter=5,
                  learning_method='online',
                  learning_offset=50.,
                  random_state=0)
        lda.fit(sub_text_tf)
        if final_result:
            tf_vectorizer._validate_vocabulary()
        tf_feature_names = tf_vectorizer.get_feature_names()
        key_words = print_top_words(lda, tf_feature_names, n_top_words)
        mega_subs_LDA.append([key_words, mega_subs[sub_id][1]])
    return mega_subs_LDA


# ### Execution of code

# In[118]:


srt_file = pysrt.open(text_file)

t_time = ""
cur_time = ""
subtitles = []
mega_subs = []
n_dialogues = 0

for dialogue in srt_file:
    subtitles.append(text_preprocessing(dialogue.text))
    cur_start = str(dialogue.start.hours)+":"+str(dialogue.start.minutes) + \
        ":"+str(dialogue.start.seconds)
    cur_end = str(dialogue.end.hours)+":"+str(dialogue.end.minutes)+":"+str(dialogue.end.seconds)

    if n_dialogues == 0:
        t_time = cur_start
    elif n_dialogues == n_slice-1:
        t_time = t_time + "," + cur_end

    n_dialogues += 1

    if n_dialogues % n_slice == 0:
        mega_subs.append([subtitles, t_time])
        t_time = ""
        subtitles = []
        n_dialogues = 0

if n_dialogues != 0:
    t_time = t_time + "," + cur_end
    mega_subs.append([subtitles, t_time])


# In[119]:


mega_subs_LDA = get_LDA_results(mega_subs)
merged_subs_LDA = []


for sub_id in range(len(mega_subs_LDA)):
    word_set = set(mega_subs_LDA[sub_id][0][0].split())
    merged_subs_LDA.append([word_set, mega_subs_LDA[sub_id][1]])

print(merged_subs_LDA[1])
# print(mega_subs[0])


# ### Bottom-Up merging on threshold

# In[120]:


merged_mega_subs = []
merged_mega_content = []

merged_mega_subs.append(merged_subs_LDA[0])
merged_mega_content.append(mega_subs[0])

ind = 1
while ind < len(merged_subs_LDA):
    #     get_sliced_audio(merged_mega_subs[-1][1])
    #     get_sliced_audio(merged_subs_LDA[ind][1])
    #     audio_similarity = get_audio_similarity(merged_mega_subs[-1][1], merged_subs_LDA[ind][1])
    audio_similarity = 0
    text_similarity = jaccard_similarity(merged_mega_subs[-1][0], merged_subs_LDA[ind][0])

    similarity = (lambda_1*text_similarity) + ((1-lambda_1)*audio_similarity)
#     print(text_similarity, audio_similarity, similarity)

    if similarity <= threshold:  # Merge both the nodes
        merged_mega_subs[-1][0].union(merged_subs_LDA[ind][0])
        merged_mega_content[-1][0] += mega_subs[ind][0]
        start = merged_mega_subs[-1][1].split(',')[0]
        end = merged_subs_LDA[ind][1].split(',')[1]
        t_time = start + ',' + end
        merged_mega_subs[-1][1] = t_time
        merged_mega_content[-1][1] = t_time
    else:  # Create a new segment
        merged_mega_content.append(mega_subs[ind])
        merged_mega_subs.append(merged_subs_LDA[ind])
    ind += 1

if os.path.exists('./audio_chunks/'):
    shutil.rmtree('./audio_chunks/')
if os.path.exists(text_file):
    os.remove(text_file)
if os.path.exists(audio_file):
    os.remove(audio_file)


# In[124]:


# print('No of segments:', len(merged_mega_content))
# # for i in merged_mega_content:
# #     print(i[1])

# print(merged_mega_content[1][0])
# print('\n')
# text = ''
# for i in merged_mega_content[1][0]:
#     text += (i+ ' ')

# summarizer = TextRankSummarizer()
# print('Textrank:', keywords(get_summary(text, summarizer), words=10))
# print('\n')

# summarizer = LexRankSummarizer()
# print('Lexrank:', keywords(get_summary(text, summarizer), words=10))
# print('\n')

# print('Gesim summarization:', keywords(text, words=10))
# print('\n')


# In[122]:


segmented_merged_LDA = get_LDA_results(merged_mega_content, n_top_words=5, final_result=False)


# In[127]:


for segment in merged_mega_content:
    text = ''
    for sen in segment[0]:
        text += (sen+' ')
    summarizer = LexRankSummarizer()
    extracted_keywords = keywords(get_summary(text, summarizer), words=5)
    print(segment[1].split(',')[0], segment[1].split(',')[1], extracted_keywords.split('\n'))


# In[ ]:
