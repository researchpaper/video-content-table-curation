package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import indexerExec.Index;

/**
 * Servlet implementation class RunIndexer_ext
 */
@WebServlet("/RunIndexer_ext")
public class RunIndexer_ext extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RunIndexer_ext() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		String url = request.getParameter("url");
		
		//save srt file
		
//		String filename = "";
//		String dirPath = ""; //where .py file is saved
//		String outFile = "./src/indexerExec/out.txt";
//		Exec exec = new Exec();
//		exec.execPython(filename, dirPath,outFile);
//		
		//read from outFile and send to jsp
		//each line in out file will correspond to one index
		//say =  separated
		// 
		
//		File fl = new File(outFile);
//		
//		BufferedReader br = new BufferedReader(new FileReader(fl));
//		String line = "";
//		ArrayList<Index> indexList = new ArrayList<Index>();
//		while((line = br.readLine())!= null)
//		{
//			String [] temp = line.split("=");
//			Index in = new Index(temp[0],temp[1]);
//			indexList.add(in);
//		}
		System.out.println(url);
		//request.setAttribute("indexList", indexList);
		//request.getRequestDispatcher("Home.jsp").forward(request,response);
		ArrayList<Index> indexList = new ArrayList<Index>();
		//indexList.add(new Index("3:45 - 5:55",url));
		//indexList.add(new Index("5:55 - 10:65","merge sort"));
		request.setAttribute("indexList", indexList);
		request.getRequestDispatcher("Home.jsp").forward(request,response);
		
	}

}
