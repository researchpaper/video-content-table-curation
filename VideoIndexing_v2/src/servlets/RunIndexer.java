package servlets;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import indexerExec.Exec;
import indexerExec.Index;
/**
 * Servlet implementation class RunIndexer
 */
@WebServlet("/RunIndexer")
public class RunIndexer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RunIndexer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		
		//save srt file
		
		String filename = "videoindexing_end_to_end.py";
		String dirPath = "D:/m.tech/Sem 3/NLP/final/";//"F:/workspaces/JavaWorkSpace/runPython/testFiles/"; //where .py file is saved
		String outFile = "F:/workspaces/JavaWorkSpace/VideoIndexing_v2/src/indexerExec/out.txt";//"C:/Users/ArpanMukherjee/Downloads/project_videoIndexer/VideoIndexing/src/indexerExec/out.txt";
		String inFile = "F:/workspaces/JavaWorkSpace/VideoIndexing_v2/src/indexerExec/in.txt"; //"C:/Users/ArpanMukherjee/Downloads/project_videoIndexer/VideoIndexing/src/indexerExec/in.txt";				
		Exec exec = new Exec();
		exec.execPython(filename, dirPath,inFile,outFile);
		
		//read from outFile and send to jsp
		//each line in out file will correspond to one index
		//say =  separated
		// 
		String url = request.getParameter("url");
		File fll = new File(inFile);
		BufferedWriter bw = new BufferedWriter(new FileWriter(inFile));
		bw.write(url);
		bw.close();
		
		
		File fl = new File(outFile);
		
		BufferedReader br = new BufferedReader(new FileReader(fl));
		String line = "";
		ArrayList<Index> indexList = new ArrayList<Index>();
		int errFlag =0;
		int first=0;
		while((line = br.readLine())!= null)
		{
			String [] temp = line.split("=");
			if(temp.length==3)
			{
				Index in = new Index(temp[0],temp[1],temp[2]);
				//Index in = new Index(line,line);
				indexList.add(in);
				first=1;
			}
			else //if(first==1)
			{
				//error occurred
				if(first==1)
					errFlag = 1;
				//Index in = new Index(temp[0],temp[1]);
				//Index in = new Index(line,line);
				//indexList.add(in);
				
			}
			
			
		}
		System.out.println(url);
		//request.setAttribute("indexList", indexList);
		//request.getRequestDispatcher("Home.jsp").forward(request,response);
//		ArrayList<Index> indexList = new ArrayList<Index>();
//		indexList.add(new Index("3:45 - 5:55",url));
//		indexList.add(new Index("5:55 - 10:65","merge sort"));
		request.setAttribute("errFlag", errFlag);
		request.setAttribute("url", url);
		request.setAttribute("indexList", indexList);
		System.out.println("dispatching ; errFlag = " + errFlag);
		request.getRequestDispatcher("Home_temp.jsp").forward(request,response);
		
		
	}

}
