package indexerExec;

// stores index objects
public class Index {
	
	private String startTime;
	private String endTime;
	private String text;
	
	public Index(String startTime,String endTime, String text)
	{
		this.startTime = startTime;
		this.endTime = endTime;
		this.text = text;
	}
	
	
	public String getStartTime() {
		return startTime;
	}


	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}


	public String getEndTime() {
		return endTime;
	}


	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}


	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	
}
