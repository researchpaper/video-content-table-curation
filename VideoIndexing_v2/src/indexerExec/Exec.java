package indexerExec;

import java.io.File;
import java.io.IOException;

public class Exec {

	//String filename = "test1.py";
	//String dirPath = "F:/workspaces/JavaWorkSpace/runPython/testFiles/";
	public void execPython(String filename, String dirPath,String inFile,String outFile)
	{
		
		File dir = new File(dirPath);
		
		
		String[] command = ("py -3 "+ filename).split("\\s");
		ProcessBuilder procBuilder = new ProcessBuilder(command);
		Process proc;
		
		procBuilder.directory(dir);
		procBuilder.redirectErrorStream(true);
        procBuilder.redirectInput(new File(inFile));
        procBuilder.redirectOutput(new File( outFile));
        try {
			proc = procBuilder.start();
			proc.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
