<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@page import="indexerExec.*,java.util.*" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/search.css" />

<title>Video Indexer</title>
</head>
<body>

<script>
function seek(url)
{
	//alert("hello " + url);
	var x = document.getElementById("youtubeFrame");
	x.src = url;
}
</script>


<div class='heading'>
	<h1>Video Indexer</h1>	
</div>

	<form class="form-wrapper cf" action="Run_Indexer" method="post">
  		<input type="text" name ="url" placeholder="Enter url for video..." />
	  	<button type="submit">Fetch Index</button>
	</form>
	<%
		
		
		
		if(request.getAttribute("indexList")!= null)
		{ 
			String ef = request.getAttribute("errFlag").toString();
			String url = request.getAttribute("url").toString();
			String [] urlId= url.split("=");
			String embedUrl = "https://www.youtube.com/embed/" +urlId[1] ; // + "?start=" + "15" ;
	  		int errFlag = Integer.parseInt(ef);
	  		System.out.println("from jsp - got attributes");
	  		System.out.println(embedUrl);
			// if(errFlag==0)
			//{	
			
	%>
	<div>
		<iframe id ="youtubeFrame" width="420" height="345" src=<%=embedUrl%>></iframe>
	</div>
		<div class="table">
    	<table id="result">
    		<tr>
    			<th>Start Time</th>
    			<th>End Time</th>
    			<th>Topics</th>
    			<th>Link</th>
    		</tr>
    	
	    	
  
		
					
				
				<%
					//String url = (String) request.getAttribute("url");
					ArrayList<Index> index = (ArrayList<Index>) request.getAttribute("indexList");
				
					for(Index in : index)
					{
						String time1 = in.getStartTime();
						String time2 = in.getEndTime();
						String topic = in.getText();
						String[] times = time1.split(":", 0);
						int t = (Integer.parseInt(times[0])*3600) + (Integer.parseInt(times[1])*60) + Integer.parseInt(times[2]);
						String topic_url = url.split("&", 0)[0] + "&t=" + Integer.toString(t) + "s";
						String topic_url_seek = embedUrl + "?start=" + Integer.toString(t);
						String funcParam = topic_url_seek ;
				%>
						 <tr>
      						<td><%=time1%>></td>
      						<td><%=time2%></td>
      						<td><%=topic%></td>
      						<td><button type="button" onclick="seek('<%=funcParam%>')">Jump to topic</button></td>
    					</tr>		
	 				
					<% 
					}
					%>
							
			
		</table> 
		</div>
		
	<%	
			//}
			//else
			//{
			//	//error occurred
			//	
			//	%>
			<!-- <div class="error">An unexpected error occurred, Please try later.</div>  -->
				
				<%
			//	
		//	}	
		}
		
	%>
</body>
</html>