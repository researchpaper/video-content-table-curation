<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="indexerExec.*,java.util.*" %>
<!doctype html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html">
  <title>Video Indexer</title>
  <meta name="author" content="Shubhi Tiwari">
  <link rel="shortcut icon" href="http://static.tmimgcdn.com/img/favicon.ico">
  <link rel="icon" href="http://static.tmimgcdn.com/img/favicon.ico">
 
  <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="js/responsiveCarousel.min.js"></script>
   <link rel="stylesheet" type="text/css" href="css/search.css" />
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
</head>

<body id = "my_body">

<script>
function seek(url)
{
	//alert("hello " + url);
	var x = document.getElementById("youtubeFrame");
	x.src = url;
}

function change_bg()
{
	var b = document.getElementById("my_body");
	b.style.backgroundImage = "url('bg8.jpg')";
	
	
	}
	
window.onload = change_bg;

</script>

<div class='katana_heading'>
	<h1>Video Indexer</h1>	
</div>
  <div id="w">
    <form class="form-wrapper cf" action="Run_Indexer" method="post">
  		<input type="text" name ="url" placeholder="Enter url for video..." />
	  	<button type="submit">Fetch Index</button>
	</form>
    
    <%
		
		
		
		if(request.getAttribute("indexList")!= null)
		{ 
			String ef = request.getAttribute("errFlag").toString();
			String url = request.getAttribute("url").toString();
			String [] urlId= url.split("=");
			String embedUrl = "https://www.youtube.com/embed/" +urlId[1] ; // + "?start=" + "15" ;
	  		int errFlag = Integer.parseInt(ef);
	  		System.out.println("from jsp - got attributes");
	  		System.out.println(embedUrl);
			// if(errFlag==0)
			//{	
			
	%>
	<div class="katana_frame">
		<iframe id ="youtubeFrame" width="600" height="320" src=<%=embedUrl%>></iframe>
	</div>
    <br></br>
    <nav class="slidernav">
      <div id="navbtns" class="clearfix">
        <a href="#" class="previous"><img src="curveLeft.png" width="20px" height="20px"></a>
        <a href="#" class="next"><img src="curveRight.png" width="20px" height="20px"></a>
        
      </div>
    </nav>
    
    <div class="crsl-items" data-navigation="navbtns">
    <div class="crsl-wrap">
    
    <%
					//String url = (String) request.getAttribute("url");
					ArrayList<Index> index = (ArrayList<Index>) request.getAttribute("indexList");
					int count = 0;
					for(Index in : index)
					{
						String time1 = in.getStartTime();
						System.out.println("time1 = "+time1);
						String time11 = time1+ "s";
						String time2 = in.getEndTime();
						System.out.println("time2 = "+time2);
						String time22 = time2 + "s";
						String topic = in.getText();
						String topic1 = topic.substring(2,topic.length()-1);
						
						String[] times = time1.split(":", 0);
						int t = (Integer.parseInt(times[0])*3600) + (Integer.parseInt(times[1])*60) + Integer.parseInt(times[2]);
						String topic_url = url.split("&", 0)[0] + "&t=" + Integer.toString(t) + "s";
						String topic_url_seek = embedUrl + "?start=" + Integer.toString(t)+"&autoplay=1";
						String funcParam = topic_url_seek ;
						count++;
				%>
    
    
    
        <div class="crsl-item">
          
          
          
          <h3><%=count%></h3><h4><%=time11%> - <%=time22 %></h4>
          
          <h4><%=topic1 %></h4>
          <!--  <p class = "katana_topic"></p> -->
          
          <div>
          	<button class= "katana_button" type="button" onclick="seek('<%=funcParam%>')">Jump to topic</button>
          </div>
          
        </div><!-- post #1 -->
       <% 
		}
		%>
		
		
      </div><!-- @end .crsl-wrap -->
    </div><!-- @end .crsl-items -->
   
			<!-- <div class="error">An unexpected error occurred, Please try later.</div>  -->
				
	<%
			//	
		//	}	
		}
		
	%>
  </div><!-- @end #w -->
<script type="text/javascript">
$(function(){
  $('.crsl-items').carousel({
    visible: 3,
    itemMinWidth: 180,
    itemEqualHeight: 370,
    itemMargin: 9,
  });
  
  $("a[href=#]").on('click', function(e) {
    e.preventDefault();
  });
});
</script>
</body>
</html>